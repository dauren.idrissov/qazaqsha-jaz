package kz.daurenidrissov.qazaqshajaz.ui

import kz.daurenidrissov.common.base.MvpPresenter
import kz.daurenidrissov.common.base.MvpView

interface MainActivityContract {

    interface View : MvpView {
        fun showSomeView()
    }

    interface Presenter : MvpPresenter<View> {
        fun load()
    }
}