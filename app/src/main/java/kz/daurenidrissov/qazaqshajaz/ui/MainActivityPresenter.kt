package kz.daurenidrissov.qazaqshajaz.ui

import kz.daurenidrissov.common.base.BasePresenter
import kz.daurenidrissov.qazaqshajaz.ui.MainActivityContract

class MainActivityPresenter : BasePresenter<MainActivityContract.View>(),
    MainActivityContract.Presenter {

    override fun load() {
        view?.showSomeView()
    }

}