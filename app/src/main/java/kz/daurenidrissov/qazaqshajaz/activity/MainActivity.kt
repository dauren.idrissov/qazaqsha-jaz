package kz.daurenidrissov.qazaqshajaz.activity

import android.os.Bundle
import kz.daurenidrissov.common.base.BaseActivity
import kz.daurenidrissov.qazaqshajaz.ui.MainActivityContract
import kz.daurenidrissov.qazaqshajaz.ui.MainActivityPresenter
import kz.daurenidrissov.common.contract.AppRouterContract
import kz.daurenidrissov.qazaqshajaz.R
import org.koin.androidx.viewmodel.ext.android.viewModel

class MainActivity : BaseActivity(), MainActivityContract.View, AppRouterContract {

    private val presenter: MainActivityPresenter by viewModel()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        showSozdik()
    }

    override fun showSozdik() {
        //replaceFragment(SozdikFragment.create())
    }

    override fun showSomeView() {

    }
}
